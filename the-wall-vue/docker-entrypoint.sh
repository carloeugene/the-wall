#!/bin/sh

APPLICATION_DIR=/app/the-wall/the-wall-vue

echo "Verifying working directory ..."
cd $APPLICATION_DIR
pwd

echo "Checking package.json ..."
ls -al $APPLICATION_DIR/package.json

echo "Starting application ..."
npm start
