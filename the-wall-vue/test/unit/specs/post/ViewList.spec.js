import Vue from 'vue'
import ViewList from '@/components/post/ViewList'
// import nock from 'nock'
import moxios from 'moxios'
import Router from 'vue-router'
import router from '@/router'

describe('post/ViewList.vue', () => {
  beforeEach(function () {
    // import and pass your custom axios instance to this method
    moxios.install()
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    moxios.uninstall()
  })
  it('should render correct contents', (done) => {
    moxios.stubRequest('/api/posts?filter[order]=createdDate DESC&filter[limit]=10', {
      status: 200,
      responseText: [
        {
          entry: 'test',
          createdDate: '2017-11-25T05:35:38.000Z',
          'id': 1
        }
      ]
    })

    moxios.stubRequest('/api/posts/1/hashtags', {
      status: 200,
      responseText: [
        {
          name: 'testTag',
          'id': 1
        }
      ]
    })
    Vue.use(Router)
    const Constructor = Vue.extend(ViewList)

    const vm = new Constructor({router}).$mount()
    moxios.wait(() => {
      expect(vm.$el.querySelector('p.post_1').textContent).to.equal('test')
      expect(vm.$el.querySelector('a#hashtag_1').textContent).to.equal('#testTag')
      done()
    })
  })
})
