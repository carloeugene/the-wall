import Vue from 'vue'
import Router from 'vue-router'

import CreatePost from '@/components/post/Create'
import ViewList from '@/components/post/ViewList'
import ViewBigScreen from '@/components/post/ViewBigScreen'
import ViewFilter from '@/components/post/ViewFilter'
import ViewPost from '@/components/post/View'
import EditPost from '@/components/post/Edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/post/create',
      name: 'CreatePost',
      component: CreatePost
    },
    {
      path: '/post/view/list',
      name: 'ViewList',
      component: ViewList
    },
    {
      path: '/bs',
      name: 'ViewBigScreen',
      component: ViewBigScreen
    },
    {
      path: '/post/view/filter/:id',
      name: 'ViewListFilter',
      component: ViewFilter
    },
    {
      path: '/post/view/:id',
      name: 'View',
      component: ViewPost
    },
    {
      path: '/post/edit/:id',
      name: 'Edit',
      component: EditPost
    },
    {
      path: '/',
      redirect: '/post/view/list'
    }
  ]
})
