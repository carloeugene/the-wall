#!/bin/sh

APPLICATION_DIR=/app/the-wall/the-wall-api

echo "Verifying working directory ..."
cd $APPLICATION_DIR
pwd

echo "Checking package.json ..."
ls -al $APPLICATION_DIR/package.json

echo "migrating database"
node ./bin/automigrate.js

echo "Starting application ..."
npm start
