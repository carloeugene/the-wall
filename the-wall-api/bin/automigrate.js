'use strict';
var path = require('path');
const fs = require('fs');
const async = require('async');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.theWallDS;

let postPath = path.resolve(__dirname, '../common/models/post.json');
let hashtagPath = path.resolve(__dirname, '../common/models/hashtag.json');

let postSchema = JSON.parse(fs.readFileSync(
  postPath,
  'utf8'
));

let hashtagSchema = JSON.parse(fs.readFileSync(
  hashtagPath,
  'utf8'
));

let models = ['post', 'hashtag', 'report'];

async.each(models,
  (model, callback) => {
    console.log(`model: ${model}`);
    ds.discoverModelProperties(model, (err, props) => {
      if (!props || props.length == 0) {
        return callback(`No model yet for ${model}! Recreating database.`);
      } else {
        callback();
      }
    });
  },
  (err) => {
    if (err) {
      console.log(err);
      ds.automigrate(function() {
        ds.disconnect();
      });
    } else {
      ds.disconnect();
    }
  });

